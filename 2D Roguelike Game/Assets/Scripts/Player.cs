﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MovingObject {

	public int wallDamage = 1;
	public int pointsPerFood = 10;
	public int pointsPerSoda = 20;
	public float restartLevelDelay = 0.5f;
    public int diseaseCured = 10;
	public Text foodText;
    public Text corruptionText;
    public Text radiationText;
    public bool isPoisoned;
    public AudioClip moveSound1;
    public AudioClip moveSound2;
    public AudioClip eatSound1;
    public AudioClip eatSound2;
    public AudioClip drinkSound1;
    public AudioClip drinkSound2;
    public AudioClip gameOverSound;
    public AudioClip playerGrunt;
    public AudioClip cure;
    public AudioClip disease;

    public int radiationTurns; // limits number of turns until death

    private Animator animator;
    private int countFiveTurns;
	private int food;
    private int diseaseProgress = 0;
    private Vector2 touchOrigin = -Vector2.one;

	// Use this for initialization
	protected override void Start () {
		animator = GetComponent<Animator> ();

		food = GameManager.instance.playerFoodPoints;
		foodText.text = "Food: " + food;

        diseaseProgress = GameManager.instance.playerDisease;
        corruptionText.text = "Disease: " + diseaseProgress + "/100";
        corruptionText.color = Color.magenta;

        countFiveTurns = 0;

        radiationTurns = GameManager.instance.playerRadiation;
        if(radiationTurns > 0)
        {
            if (radiationTurns <= 10)
                radiationText.color = Color.red;
            else if (radiationTurns > 10 && radiationTurns <= 20)
                radiationText.color = Color.yellow;
            radiationText.text = "Turns Left: " + radiationTurns;
        }
        else
            radiationText.text = "";

		base.Start ();
	}

	private void OnDisable() {
		GameManager.instance.playerFoodPoints = food;
        GameManager.instance.playerDisease = diseaseProgress;
        GameManager.instance.playerRadiation = radiationTurns;
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameManager.instance.playersTurn)
			return;
		int horizontal = 0;
		int vertical = 0;

#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR

        horizontal = (int)Input.GetAxisRaw ("Horizontal");
		vertical = (int)Input.GetAxisRaw ("Vertical");

		if (horizontal != 0)
			vertical = 0;

#else
        if(Input.touchCount > 0) {
            Touch myTouch = Input.touches[0];

            if(myTouch.phase == TouchPhase.Began) {
                touchOrigin = myTouch.position;
            }
            else if(myTouch.phase == TouchPhase.Ended && touchOrigin.x >= 0) {
                Vector2 touchEnd = myTouch.position;
                float x = touchEnd.x - touchOrigin.x;
                float y = touchEnd.y - touchOrigin.y;
                touchOrigin.x = -1;
                if (Mathf.Abs(x) > Mathf.Abs(y))
                    horizontal = x > 0 ? 1 : -1;
                else
                    vertical = y > 0 ? 1 : -1;
            }
        }

#endif

        if (horizontal != 0 || vertical != 0)
			AttemptMove<Wall> (horizontal, vertical);
	}

	protected override void AttemptMove <T> (int xDir, int yDir) {

		food--;

        if (radiationTurns > 0)
        {
            radiationTurns--;
            radiationText.text = "Turns Left: " + radiationTurns;
        }

        // reset color after getting hit
        if (foodText.color.Equals(Color.red))
            foodText.color = Color.white;

		foodText.text = "Food: " + food;

        if (countFiveTurns == 5) {
            diseaseProgress += 3;
            countFiveTurns = 0;
            corruptionText.text = "Disease: " + diseaseProgress + "/100";
        }
        else
            countFiveTurns++;

		base.AttemptMove <T> (xDir, yDir);
		RaycastHit2D hit;
        if (Move(xDir, yDir, out hit))  {
            SoundManager.instance.RandomizeSfx(moveSound1, moveSound2);
        }

        if (radiationTurns < 0)
            radiationText.text = "";

		CheckIfGameOver ();
		GameManager.instance.playersTurn = false;
	}

	private void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "Exit")
        {
            Invoke("Restart", restartLevelDelay);
            enabled = false;
        }
        else if (other.tag == "Food")
        {
            food += pointsPerFood;
            SoundManager.instance.RandomizeSfx(eatSound1, eatSound2);
            foodText.text = "Food: " + food;
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Soda")
        {
            food += pointsPerSoda;
            SoundManager.instance.RandomizeSfx(drinkSound1, drinkSound2);
            foodText.text = "Food: " + food;
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Cure")
        {
            diseaseProgress = 0;
            corruptionText.text = "Disease: " + diseaseProgress + "/100";
            SoundManager.instance.PlaySingle(cure);
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Disease")
        {
            diseaseProgress += 15;
            corruptionText.text = "Disease: " + diseaseProgress + "/100";
            SoundManager.instance.PlaySingle(disease);
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "CureRadiation")
        {
            isPoisoned = false;
            if (radiationTurns < 0)
            {
                other.gameObject.SetActive(false);
                return;
            }
            radiationTurns = -1;
            if (!radiationText.text.Equals("Cured!"))
            {
                radiationText.text = "Cured!";
                radiationText.color = Color.green;
            }
            SoundManager.instance.PlaySingle(cure);
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "MedRadiation")
        {
            if (!(radiationTurns <= 20 && radiationTurns > 0))
                radiationTurns = 20;
            if (radiationTurns > 0)
            {
                radiationText.text = "Turns Left: " + radiationTurns;
                if (radiationTurns > 10)
                    radiationText.color = Color.yellow;
            }
            SoundManager.instance.PlaySingle(disease);
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "StrongRadiation")
        {
            if (!(radiationTurns <= 10 && radiationTurns > 0))
            {
                radiationTurns = 10;
            }
            if (radiationTurns > 0)
            {
                radiationText.text = "Turns Left: " + radiationTurns;
                if (radiationTurns <= 10)
                    radiationText.color = Color.red;
            }
            SoundManager.instance.PlaySingle(disease);
            other.gameObject.SetActive(false);
        }
	}

	protected override void OnCantMove <T> (T component) {
		Wall hitWall = component as Wall;
		hitWall.DamageWall (wallDamage);
		animator.SetTrigger ("playerChop");
	}

	private void Restart() {
		SceneManager.LoadScene("Main");
	}

	public void HitByZombie(int foodLoss) {
		animator.SetTrigger ("playerHit");

		food -= foodLoss;
        foodText.text = "Food: " + food;

        diseaseProgress += 20;
        corruptionText.text = "Disease: " + diseaseProgress + "%";

        foodText.color = Color.red;
		CheckIfGameOver ();
	}

	private void CheckIfGameOver() {
        if (food <= 0 || radiationTurns == 0 || diseaseProgress >= 100) {
            SoundManager.instance.PlaySingle(gameOverSound);
            SoundManager.instance.musicSource.Stop();       
            GameManager.instance.GameOver();
        }
	}
}
