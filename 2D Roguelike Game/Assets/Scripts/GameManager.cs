﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public float turnDelay = .1f;
	public static GameManager instance = null;
	public BoardManager boardScript;
	public int playerFoodPoints = 100;
    public int playerHealth = 100;
    public int playerDisease = 0;
    public int playerRadiation = -1;
	[HideInInspector] public bool playersTurn = true;
	public float levelStartDelay = 1.5f;

	public int level = 0;
	private List<Enemy> enemies;
	private bool enemiesMoving;
	private Text levelText;
	private GameObject levelImage;
    private GameObject levelRestart;
	private bool doingSetup;

	// Use this for initialization
	void Awake () {
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
		DontDestroyOnLoad (gameObject);
		enemies = new List<Enemy> ();
		boardScript = GetComponent<BoardManager> ();
		//InitGame ();
	}

	void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode) {
		level++;
		InitGame ();
	}

	void OnEnable() {
		//listen for scene change
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDisable() {
		SceneManager.sceneLoaded -= OnLevelFinishedLoading;
	}

	void InitGame() {
		doingSetup = true;

		levelImage = GameObject.Find ("LevelImage");
		levelText = GameObject.Find ("LevelText").GetComponent<Text> ();
		levelText.text = "Day " + level;
		levelImage.SetActive (true);
		Invoke ("HideLevelImage", levelStartDelay);

        levelRestart = GameObject.Find("LevelRestart");
        levelRestart.SetActive(false);
        Invoke("HideLevelRestart", levelStartDelay);

		enemies.Clear ();
		boardScript.SetupScene (level);
	}

	private void HideLevelImage() {
		levelImage.SetActive (false);
		doingSetup = false;
	}

    private void HideLevelRestart()
    {
        levelRestart.SetActive(false);
        doingSetup = false;
    }

	public void GameOver() {
		levelText.text = "After " + level + " days, you died...";
		levelImage.SetActive (true);
        levelRestart.SetActive(true);
	}
	
	// Update is called once per frame
	public void Update () {
		if (playersTurn || enemiesMoving || doingSetup)
			return;
		StartCoroutine (MoveEnemies ());
	}

	public void AddEnemyToList(Enemy script) {
		enemies.Add (script);
	}

	IEnumerator MoveEnemies() {
		enemiesMoving = true;
		yield return new WaitForSeconds (turnDelay);
		if (enemies.Count == 0) {
			yield return new WaitForSeconds (turnDelay);
		}
		for (int i = 0; i < enemies.Count; i++) {
			enemies [i].MoveEnemy ();
			yield return new WaitForSeconds (enemies [i].moveTime);
		}
		playersTurn = true;
		enemiesMoving = false;
	}
}
