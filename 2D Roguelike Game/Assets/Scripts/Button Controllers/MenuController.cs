﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public Button startText;
    public Button controlsText;
    public Button quitText;
    public Canvas quitMenu;

	// Use this for initialization
	void Start () {
        quitMenu = quitMenu.GetComponent<Canvas>();
        startText = startText.GetComponent<Button>();
        controlsText = controlsText.GetComponent<Button>();
        quitText = quitText.GetComponent<Button>();
        quitMenu.enabled = false;
	}

    public void OnQuitClicked()
    {
        controlsText.enabled = false;
        startText.enabled = false;
        quitText.enabled = false;
        quitMenu.enabled = true;
    }
    public void OnNoClicked()
    {
        controlsText.enabled = true;
        startText.enabled = true;
        quitText.enabled = true;
        quitMenu.enabled = false;
    }
	
    public void StartGame()
    {
        SceneManager.LoadScene("Main");
    }

    public void ShowControls()
    {
        SceneManager.LoadScene("Controls");
    }

    public void CallQuitGame()
    {
        Application.Quit();
    }

	// Update is called once per frame
	void Update () {
		
	}
}
