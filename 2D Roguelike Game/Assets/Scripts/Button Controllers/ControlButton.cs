﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlButton : MonoBehaviour {

    public Button mainMenu;

	// Use this for initialization
	void Start () {
        mainMenu = mainMenu.GetComponent<Button>();
	}

    public void ToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
